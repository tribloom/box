package stanford.edu.alfresco_box;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.SecondaryType;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.ObjectIdImpl;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.PropertyData;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.SystemUtils;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIConnectionListener;
import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxMetadataFilter;
import com.box.sdk.BoxSearch;
import com.box.sdk.BoxSearchParameters;
import com.box.sdk.BoxSharedLink;
import com.box.sdk.Metadata;
import com.box.sdk.MetadataTemplate;
//import com.box.sdk.URLTemplate;
import com.box.sdk.MetadataTemplate.Field;
import com.box.sdk.PartialCollection;

/**
 * Hello world!
 *
 */
public class App {
	private static String ALFRESCO_URL = "http://54.202.234.8:8080";
	private static Node rootNode;

	private static Map<String, String> mapping = new HashMap<String, String>();
	private static Map<String, String> mappingCases = new HashMap<String, String>();
	private static Map<String, String> mappingError = new HashMap<String, String>();

	private static String[] bool = { "Yes", "No" };
	private static String[] cwoAccess = { "Not for Distribution", "Stanford GSB classes only, restricted",
			"Stanford GSB classes only, accessible", "Instructors Only", "Educational Use Only",
			"Education and Corporate use", "Free" };
	private static Map<String, BoxMetadata> caseMapping = new HashMap<String, BoxMetadata>();
	private static Map<String, BoxMetadata> syllabiMapping = new HashMap<String, BoxMetadata>();
	public String accessToken = null;
	public String refreshToken = null;
	private static String boxClientId = "opuitnahtbvfefalzhzav9b9e2k1jnbr";
	private static String boxClientSecret = "GET5iaqeYIop7ETZSuu3tj6lz3zulMJK";
	private static final int ITEMS_PER_PAGE = 100;
	private static final String BOX_AUTH_FILE = "boxAuth.txt";
//	private static final String BOX_AUTH_FILE_PATH = "/home/ec2-user/";
//	private static final String BOX_AUTH_FILE_PATH = "C:\\Users\\Michael\\Downloads\\";
	private static final String BOX_AUTH_FILE_PATH_LINUX = "/home/ec2-user/";
	private static final String BOX_AUTH_FILE_PATH_WIN = "C:\\Users\\Michael\\Downloads\\";
	private static String BOX_AUTH_FILE_PATH = BOX_AUTH_FILE_PATH_WIN;
	private static final long MAX_FILE_SIZE = 5000000000L; // bytes of 5 GB


	public static void initMapping() throws Exception {

		caseMapping.put("cm:title", new BoxMetadata("/title"));
		caseMapping.put("cm:description", new BoxMetadata("/description"));
		caseMapping.put("cmis:contentStreamMimeType", new BoxMetadata("/mimetype"));
		caseMapping.put("cm:author", new BoxMetadata("/author"));
		// caseMapping.put(, new BoxMetadata()); // TAGS
		caseMapping.put("gsb:cwoAbstract", new BoxMetadata("/abstract"));
		caseMapping.put("gsb:cwoItemId", new BoxMetadata("/itemId1"));
		caseMapping.put("gsb:cwoPublicationYear", new BoxMetadata("/publicationYear"));
		caseMapping.put("gsb:cwoProductType", new BoxMetadata("/productType"));
		caseMapping.put("gsb:permisiionStatement", new BoxMetadata("/permissionsStatement"));
		caseMapping.put("gsb:cwoPaperCopy", new BoxMetadata("/availableToTheGsbCommunity", bool));
		caseMapping.put("gsb:cwoFreeCase", new BoxMetadata("/freeCase1", bool));
		caseMapping.put("gsb:cwoAccess", new BoxMetadata("/caseStudyAccess1", cwoAccess));
		caseMapping.put("gsb:cwoAcademicArea", new BoxMetadata("/academicArea2"));
		caseMapping.put("gsb:cwoBusinessTopics", new BoxMetadata("/businessTopics1"));
		caseMapping.put("gsb:cwoHbpDiscipline", new BoxMetadata("/hbpDiscipline1"));
		caseMapping.put("gsb:cwoLinkAtHarvard", new BoxMetadata("/linkAtHarvard"));
		caseMapping.put("gsb:cwoSource", new BoxMetadata("/source"));
		caseMapping.put("gsb:cwoLength", new BoxMetadata("/length"));
		caseMapping.put("gsb:cwoLearningObjective", new BoxMetadata("/learningObjective"));
		caseMapping.put("gsb:cwoSetting", new BoxMetadata("/industrySetting"));
		caseMapping.put("gsb:cwoECopy", new BoxMetadata("/theCaseCenterLink"));
		caseMapping.put("gsb:cwoCitation", new BoxMetadata("/citation"));
		caseMapping.put("gsb:authorsSunetId", new BoxMetadata("/authorsSunetidCommaSeparatedForMultipleAuthors"));
		caseMapping.put("cmis:creationDate", new BoxMetadata("/alfrescoCreatedDate"));

		syllabiMapping.put("cm:title", new BoxMetadata("/title"));
		syllabiMapping.put("cmis:contentStreamMimeType", new BoxMetadata("/mimetype"));
		syllabiMapping.put("cm:author", new BoxMetadata("/author"));
		syllabiMapping.put("gsb:courseName", new BoxMetadata("/courseName"));
		syllabiMapping.put("gsb:subjectArea", new BoxMetadata("/subjectArea"));
		syllabiMapping.put("gsb:catalogNumber", new BoxMetadata("/catalogNumber"));
		syllabiMapping.put("gsb:courseSection", new BoxMetadata("/courseSection"));
		syllabiMapping.put("gsb:quarter", new BoxMetadata("/quarter"));
		syllabiMapping.put("gsb:year", new BoxMetadata("/year1"));
		syllabiMapping.put("gsb:documentType", new BoxMetadata("/type"));
		syllabiMapping.put("gsb:facultyNames", new BoxMetadata("/facultyNames"));
		syllabiMapping.put("gsb:psSectionName", new BoxMetadata("/pssectionnameEg1134acct21001"));
	}

	public static void main(String[] args) throws Exception {
		String user = "admin-tribloom";
		String password = "tr7b00n";
		String url = "http://localhost:8080/alfresco/api/-default-/public/cmis/versions/1.1/atom";
		String repositoryId = "-default-";
		String docPath = "";
		String boxKey = "";
		String boxFolder = "";
		if (args.length > 0) {
			user = args[0];
		}
		if (args.length > 1) {
			password = args[1];
		}
		if (args.length > 2) {
			url = args[2];
		}
		if (args.length > 3) {
			repositoryId = args[3];
		}
		if (args.length > 4) {
			docPath = args[4];
		}
		if (args.length > 5) {
			boxKey = args[5];
		}
		if (args.length > 6) {
			boxFolder = args[6];
		}
		System.out.println("Connecting to " + url + " with user '" + user + "' using password '" + password + "'");
		System.out.println("Repository ID: " + repositoryId);
		System.out.println("Doc Path: " + docPath);

		// Exit if no document path
		if (docPath == null || "".equals(docPath)) {
			return;
		}
		
		String os = System.getProperty("os.name");
		Boolean isWin = SystemUtils.IS_OS_WINDOWS;
		System.out.println("Operating System: "+os+" is windows? "+isWin);
		if(isWin) {
			BOX_AUTH_FILE_PATH = BOX_AUTH_FILE_PATH_WIN;
		} else {
			BOX_AUTH_FILE_PATH = BOX_AUTH_FILE_PATH_LINUX;
		}

		initMapping();

		// String id = findBoxFolderId(new
		// BoxAPIConnection(boxKey),"GSB Box for IT Operations (Workgroup)/Case Writing/TEST",
		// null);
		// System.out.println("Id: "+id);

//		 box(boxKey);

		// List<String> tags = getTags(null);
		// for(String tag : tags) {
		// System.out.println("Tag: "+tag);
		// }

		// if(true) {
		// return;
		// }

		// box(boxKey);
		// App a = new App();

		// get a CMIS session
		Session session = connectCMIS(user, password, url, repositoryId);
		
		///
//		String cql = "select d.*, o.* from cmis:document as d join gsb:casewriting as o on d.cmis:objectId = o.cmis:objectId where (o.gsb:cwoAccess = 'Education and Corporate use' or o.gsb:cwoAccess = 'Educational use only' or o.gsb:cwoAccess = 'Instructors only') and (o.gsb:cwoLinkAtHarvard IS NOT NULL or o.gsb:cwoECopy IS NOT NULL)";
//		ItemIterable<QueryResult> results = queryCMIS(session, cql);
//		String FILE_NAME_B = "2018-02-13-CaseWriting-case-mapping.csv";
//		String FILE_PATH = "C:\\Users\\Michael\\Downloads\\";
//		CSVParser pB = readCSV(FILE_PATH + FILE_NAME_B);
//		List<CSVRecord> recordsB = pB.getRecords();
//		printCMISResults(results, session, recordsB);
//		if(true) 
//			return;
		///
		
		

		// Get the base folder given by the path
		AlfrescoFolder f = (AlfrescoFolder) session.getObjectByPath(docPath);

		BoxAPIConnection api = new BoxAPIConnection(boxKey);
		try {
//			final App a = new App();
//			a.readAuthInfo();
//			BoxAPIConnection api = new BoxAPIConnection(boxClientId, boxClientSecret, a.accessToken, a.refreshToken);
//			api.addListener(new BoxAPIConnectionListener() {
//				public void onRefresh(BoxAPIConnection api) {
//					String newAccessToken = api.getAccessToken();
//					String newrefreshToken = api.getRefreshToken();
//					System.out.println("AccessToken: " + newAccessToken);
//					System.out.println("RefreshToken: " + newrefreshToken);
//					// a = new App();
//					a.accessToken = newAccessToken;
//					a.refreshToken = newrefreshToken;
//					a.writeAuthInfo();
//					// update new access and refresh token in DB/property
//				}
//
//				public void onError(BoxAPIConnection api, BoxAPIException error) {
//					System.out.println("Error in Box account details. " + error.getMessage());
//				}
//			});
//			System.out.println("Completed Box authentication");

//			 Print template info
//			 printTemplateInfo(api);
//			 if(true) {
//			 return;
//			 }

			//createSharedLink(api, "257606308601");
			// transfer to box
			moveToBox(session, f, api, boxFolder);
			// See what files are missing from box
//			getDocs(session, f, api, boxFolder);

//		} catch (BoxAPIException be) {
//			System.out.println("BOX EXCEPTION: " + be.getResponse());
//			throw be;
		} finally {
			writeMappingLocal(mapping, "mapping", true);
			writeMappingLocal(mappingCases, "case-mapping", true);
			writeMappingLocal(mappingError, "error-mapping", true);
		}
	}
	
	public static CSVParser readCSV(String fileName) {
		try {
			File file = new File(fileName);
			CSVParser parser = CSVParser.parse(file, Charset.defaultCharset(), CSVFormat.EXCEL);
			return parser;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void createSharedLink(BoxAPIConnection api, String fileId) {
//		findBoxFileId(api, "GSB BOX (Production)/Syllabi in Course Listings/2018 Spring", "STRAMGT510.01_SP16-17_SYL_Grousbeck", null);
	    BoxFile file = new BoxFile(api, fileId);
	    BoxSharedLink.Permissions permissions = new BoxSharedLink.Permissions();
	    permissions.setCanDownload(true);
	    permissions.setCanPreview(true);
//	    Date date = new Date();

//	    Calendar unshareAt = Calendar.getInstance();
//	    unshareAt.setTime(date);
//	    unshareAt.add(Calendar.DATE, 14);

	    BoxSharedLink sharedLink = file.createSharedLink(BoxSharedLink.Access.COMPANY, null, permissions);
	    System.out.println("Shared Link: "+sharedLink.getURL());
	    //BoxFile.Info info = file.getInfo();
	    //BoxSharedLink link = info.getSharedLink();
	    //System.out.println("Shared Link 2: "+link.getURL());
	    
		
	}

	public void writeAuthInfo() {
		File file = new File(BOX_AUTH_FILE_PATH+BOX_AUTH_FILE);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file, false);
			if (accessToken != null) {
				fos.write(accessToken.getBytes());
				fos.write("\n".getBytes());
			}
			if (refreshToken != null) {
				fos.write(refreshToken.getBytes());
			}
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readAuthInfo() {
		try {
			FileReader file = new FileReader(BOX_AUTH_FILE_PATH+BOX_AUTH_FILE);
			BufferedReader br = new BufferedReader(file);

			int i = 0;
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println("line: " + line);
				if (i == 0) {
					accessToken = line;
				} else if (i == 1) {
					refreshToken = line;
				}
				i++;
			}
			br.close();
			file.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeMappingLocal(Map<String, String> mapping, String filename, boolean append)
			throws IOException {
		if (mapping.size() > 0) {
			// String filename = "mapping";
			File file = new File(BOX_AUTH_FILE_PATH+filename+".csv");
			if (!append) {
				int i = 1;
				while (file.exists()) {
					file = new File(BOX_AUTH_FILE_PATH+filename+i+".csv");
					i++;
				}
			}
			FileOutputStream fos = new FileOutputStream(file, append);
			String mappingStr = mappingToString(mapping);

			fos.write(mappingStr.getBytes());
			fos.close();
		}
	}

	public static String mappingToString(Map<String, String> mapping) {
		StringBuffer sb = new StringBuffer();
		Set<String> keys = mapping.keySet();
		int length = keys.size();
		int i = 0;
		for (String key : keys) {
			sb.append(mapping.get(key));
			if (i < length) {
				sb.append("\n");
			}
			i++;
		}
		return sb.toString();
	}

	/**
	 * Move a folder to box (recursively, depth first) and update metadata if
	 * the file already exists.
	 */
	public static void moveToBox(Session session, AlfrescoFolder folder, BoxAPIConnection api, String boxPath) {
		System.out.println("--------------------------------------");
		System.out.println("Move to Box:");
		System.out.println("boxPath: " + boxPath);
		// Get all folders under the given path
		if(folder.getName().equals("197cd2e5-343c-48bf-98c6-39af6fe5825b")) {
			return;
		}
		//
//		GregorianCalendar gc = folder.getLastModificationDate();
//		SimpleDateFormat sdf = new SimpleDateFormat();
//		System.out.println("date: "+sdf.format(gc.getTime()));
		//
		ItemIterable<QueryResult> subFolders = getFolders(session, folder);
		int page = 0;
		do {
			System.out.println("Folders: " + subFolders.getPageNumItems());
			System.out.println("Folder Total: " + subFolders.getTotalNumItems());
			if (subFolders.getHasMoreItems()) {
				System.out.println("More Folders");
				System.out.println("Folders/page: " + ITEMS_PER_PAGE);
				System.out.println("Folder Page: " + page);
				subFolders = subFolders.skipTo(ITEMS_PER_PAGE * page).getPage();
			}
			int item = 1;
			for (QueryResult result : subFolders) {
				CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId")
						.getFirstValue()));
				moveToBox(session, (AlfrescoFolder) o, api, boxPath + "/"
						+ result.getPropertyById("cmis:name").getFirstValue());
				item++;
			}
			page++;
		} while (subFolders.getHasMoreItems());
			

		// When no more folders, get all documents
		ItemIterable<QueryResult> files = getFiles(session, folder);
//		int count = 0;
//		for(QueryResult result : files) {
//			System.out.println("File["+count+"]"+result.getPropertyById("cmis:objectId").getFirstValue()+"\n");
//			CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId").getFirstValue()));
//			System.out.print("File["+count+"]"+o.getProperty("cmis:name").getFirstValue()+"\n");
//			count++;
//		}
		String folderId = null;
		// if(files.getHasMoreItems()) {
		// Developer Token Auth
		// BoxAPIConnection api = new BoxAPIConnection(boxKey);
		// Oauth Auth
		// BoxAPIConnection api = new
		// BoxAPIConnection("opuitnahtbvfefalzhzav9b9e2k1jnbr","GET5iaqeYIop7ETZSuu3tj6lz3zulMJK","sjy4G3oJv8w02JORXaEI33dZU1FZg6JI",
		// "ljGsC1eNWyTStbv3nkKB1vz6C0ixTOIyOBUZRJjuYBLM31zOpoSJGK5wdi7hAirx");

		// try {
		// } catch (Exception e) {
		// System.out.println("Error in Box authentication. Error msg : " +
		// e.getMessage());
		// }
		// BoxFolder rootFolder = BoxFolder.getRootFolder(api);
		// long t1 = getTime();
		folderId = findBoxFolderId(api, boxPath, null, null);
		page = 0;
		do {
			System.out.println("Items: " + files.getPageNumItems());
			System.out.println("Total: " + files.getTotalNumItems());
			if (files.getHasMoreItems()) {
				System.out.println("More Items");
				System.out.println("Items/page: " + ITEMS_PER_PAGE);
				System.out.println("Page: " + page);
				files = files.skipTo(ITEMS_PER_PAGE * page).getPage();
			}
			int item = 1;
			for (QueryResult result : files) {
				System.out.println("CMIS Object Id: ["+result.getPropertyById("cmis:objectId").getFirstValue()+"]");
				CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId").getFirstValue()));
				System.out.println(item + ": Transfer to " + folderId + " document "
						+ o.getProperty("cmis:name").getFirstValue());
				if (!o.getProperty("cmis:name").getFirstValue().equals("SM 232 Video TRT 19min52sec.mp4")
						&& !o.getProperty("cmis:name").getFirstValue().equals("POLECON332_F01-02_SYL_Savage.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("GSBGEN380_ SP12-13_SYL_Lietz.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("OB661_SP99-00_SYL_Jost.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("SM278 - Interview with Peter Terium.mp4")) {
					// TODO move try catch to here for box api exceptions
					transferToBoxOrUpdateMetadata(api, folderId, (Document) o, boxPath);
				} 
//				} else {
//					System.out.println("Skipping "+o.getProperty("cmis:name").getFirstValue()
//							+", date "+SimpleDateFormat.getInstance().format(d)+" is before "
//							+SimpleDateFormat.getInstance().format(d2));
//				}
				item++;
			}

			page++;
		} while (files.getHasMoreItems());
	}

	public static long getTime() {
		Calendar c = Calendar.getInstance();
		return c.getTimeInMillis();
	}

	/**
	 * Get all folders under the given folder.
	 * 
	 * @param session
	 * @param f
	 * @return
	 */
	public static ItemIterable<QueryResult> getFolders(Session session, AlfrescoFolder f) {
		// Get all folders under that folder
		String cql = "SELECT * FROM cmis:folder WHERE IN_FOLDER('" + f.getId() + "')";
		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(ITEMS_PER_PAGE);
		ItemIterable<QueryResult> results = session.query(cql, false);
		return results;
	}

	/**
	 * Get all files under the given folder.
	 * 
	 * @param session
	 * @param f
	 * @return
	 */
	public static ItemIterable<QueryResult> getFiles(Session session, AlfrescoFolder f) {
		// Get all documents under that folder
		String cql = "SELECT * FROM cmis:document WHERE IN_FOLDER('" + f.getId() + "')";
		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(ITEMS_PER_PAGE);

		ItemIterable<QueryResult> results = session.query(cql, false, oc);
		return results;
	}

	public static List<String> getTags(String objectId) {
		List<String> tags = null;
		String id = objectId.substring(0, objectId.indexOf(";"));
		String url = ALFRESCO_URL+"/alfresco/service/api/node/workspace/SpacesStore/" + id
				+ "/tags";
		String response = doRequest(url);

		tags = parseResponse(response);

		return tags;
	}

	public static List<String> parseResponse(String response) {
		List<String> tags = new ArrayList<String>();

		JSONParser parser = new JSONParser();

		try {
			JSONArray arr = (JSONArray) parser.parse(response);
			@SuppressWarnings("unchecked")
			Iterator<String> i = arr.iterator();
			while (i.hasNext()) {
				tags.add(i.next());
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tags;
	}

	public static String doRequest(String url) {
		String responseStr = null;
		HttpClient client = new HttpClient();
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(new AuthScope("54.202.234.8", 8080, "realm"),
				new UsernamePasswordCredentials("admin-tribloom", "tr7b00n"));

		System.out.println("URL: " + url);
		HttpMethod method = new GetMethod(url);
		method.setDoAuthentication(true);

		try {
			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			byte[] responseBody = method.getResponseBody();

			// Deal with the response.
			// Use caution: ensure correct character encoding and is not binary
			// data
			System.out.println(new String(responseBody));
			responseStr = new String(responseBody);

		} catch (HttpException e) {
			System.err.println("Fatal protocol violation: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Fatal transport error: " + e.getMessage());
			e.printStackTrace();
		} finally {
			// Release the connection.
			method.releaseConnection();
		}
		return responseStr;
	}

	/**
	 * Get a CMIS session and return it.
	 * 
	 * @param user
	 * @param password
	 * @param url
	 * @param repositoryId
	 * @return
	 */
	public static Session connectCMIS(String user, String password, String url, String repositoryId) {
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();
		// parameter.put(SessionParameter.ATOMPUB_URL, url);
		// parameter.put(SessionParameter.BINDING_TYPE,
		// BindingType.ATOMPUB.value());
		parameter.put(SessionParameter.BROWSER_URL, url);
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.BROWSER.value());
		parameter.put(SessionParameter.AUTH_HTTP_BASIC, "true");
		parameter.put(SessionParameter.USER, user);
		parameter.put(SessionParameter.PASSWORD, password);
		parameter.put(SessionParameter.REPOSITORY_ID, repositoryId);
		parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");
		List<Repository> repositories = factory.getRepositories(parameter);
		Session session = repositories.get(0).createSession();
		System.out.println("Connected to repository:" + repositories.get(0).getId());
		return session;
	}

	public static ItemIterable<QueryResult> queryCMIS(Session session, String cql) {
		OperationContext oc = new OperationContextImpl();
		// oc.setMaxItemsPerPage(250000);
		oc.setMaxItemsPerPage(2);
		ItemIterable<QueryResult> results = session.query(cql, false);
		return results;
	}

	public static void printCMISResults(ItemIterable<QueryResult> results, Session session, List<CSVRecord> records) {
		int page = 0;
		do {
			System.out.println("Items: " + results.getPageNumItems());
			System.out.println("Total: " + results.getTotalNumItems());
			if (results.getHasMoreItems()) {
				System.out.println("More Items");
				System.out.println("Items/page: " + ITEMS_PER_PAGE);
				System.out.println("Page: " + page);
				results = results.skipTo(ITEMS_PER_PAGE * page).getPage();
			}
			int item = 1;
			for (QueryResult result : results) {
				CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId")
						.getFirstValue()));
				String outStr;
				outStr = "\"" + o.getName() + "\"";
				for (PropertyData<?> prop : o.getProperties()) {
					if (prop.getQueryName().equals("gsb:cwoAccess") || prop.getQueryName().equals("cm:title")) {
						// System.out.println(prop.getQueryName() + ": " +
						// prop.getFirstValue());
						// System.out.println(prop.getFirstValue());
						outStr += ",\"" + prop.getFirstValue() + "\"";
					}
				}
				for (CSVRecord r : records) {
					if (o.getName().equals(r.get(0))) {
						outStr += "," + r.get(2);
					}
				}

				System.out.println(outStr);
//				System.out.println("CMIS Object Id: [" + result.getPropertyById("cmis:objectId").getFirstValue() + "]");
				item++;
			}
			page++;
			// }

			// System.out.println("--------------------------------------");
		} while (results.getHasMoreItems());

		System.out.println("--------------------------------------");
		System.out.println("Total number: " + results.getTotalNumItems());
		System.out.println("Has more: " + results.getHasMoreItems());
		System.out.println("--------------------------------------");
	}

	// Check the document to see if it has the gsb:casewriting aspect or the
	// gsb:coursematerials aspect
	// Apply gsbitops metadata to casewriting and gsbSyllabi to coursematerials
	public static void transferToBoxOrUpdateMetadata(BoxAPIConnection api, String folderId, Document doc, String path) {
		int attempt = 1;
		while (true) {
			String fileId = null;
			try {
				System.out.println("Document: " + doc.getName());

				// get the folder to put the file in
				BoxFolder folder = new BoxFolder(api, folderId);
				String title = (String) doc.getProperty("cmis:name").getFirstValue();
				// Need to add file extension (based on mimetype) for files
				// without an extension
				// there is no one to one mapping for mimetype to extension, for
				// now just add .pdf
				if ((title.length() > 4 && !(title.charAt(title.length() - 4) == '.')) || !title.contains(".")) {
					String mimetype = (String) doc.getProperty("cmis:contentStreamMimeType").getFirstValue();
					if ("application/pdf".equals(mimetype) || "application/octet-stream".equals(mimetype)) {
						title = title + ".pdf";
					}
				}

				// check to see if file exists
				fileId = findBoxFileId(api, "", title, folder);
				// String fileId = findBoxFileId(api, path, title, folder);
				boolean created = false;
				BoxFile.Info newFileInfo = null;
				if (fileId == null) {
					created = true;
					// create the file
					// Get the object content stream and write
					if (doc.getContentStream() == null) {
						mappingError.put(
								(String) doc.getProperty("alfcmis:nodeRef").getFirstValue(),
								(String) doc.getProperty("cmis:name").getFirstValue() + ","
										+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);

						// For working (research) papers only ? add a
						// placeholder pdf
						FileInputStream is = new FileInputStream(BOX_AUTH_FILE_PATH + "placeholder.pdf");
						/**
						 * Temporarily disable for testing on Syllabi 4/19/2018
						 */
						newFileInfo = folder.uploadFile(is, title);
						System.out.println("empty file: " + newFileInfo.getID());

						// return;
					} else {
						BigInteger length = (BigInteger) doc.getProperty("cmis:contentStreamLength").getFirstValue();
						System.out.println("File length: " + length);
						long docSize = length.longValue();

						InputStream input = doc.getContentStream().getStream();
						// long docSize = doc.getContentStream().getLength();
						final long LARGE_FILE_SIZE = 50000000; // bytes or 50 MB
						final long MAX_FILE_SIZE = 5000000000L; // bytes of 5 GB
						System.out.println("Uploading file of size " + docSize + " B");
						if (docSize > MAX_FILE_SIZE) {
							mappingError.put(
									(String) doc.getProperty("alfcmis:nodeRef").getFirstValue(),
									doc.getProperty("cmis:name").getFirstValue() + ","
											+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + ","
											+ fileId);
							return;
						} else {
							/**
							 * Temporarily disable for testing on Syllabi 4/19/2018
							 */
							newFileInfo = folder.uploadFile(input, title);
						}

						input.close();
					}
					fileId = newFileInfo.getID();

					// metadata = getMetadata(caseMapping, doc);
					List<SecondaryType> aspects = doc.getSecondaryTypes();
					Metadata metadata = new Metadata();
					BoxFile file = new BoxFile(api, fileId);
					for (SecondaryType aspect : aspects) {
						if (aspect.getId().equals("P:gsb:casewriting")) {
							System.out.println(" " + aspect.getDisplayName() + " (" + aspect.getId() + ")");
							metadata = getMetadata(caseMapping, doc);
							/**
							 * Temporarily disable for testing on Syllabi 4/19/2018
							file.createMetadata("gsbitops", "enterprise_86433", metadata);
							*/
						} else if (aspect.getId().equals("P:gsb:coursematerials")) {
							System.out.println(" " + aspect.getDisplayName() + " (" + aspect.getId() + ")");
							metadata = getMetadata(syllabiMapping, doc);
							/**
							 * Temporarily disable for testing on Syllabi 4/19/2018
							*/
							file.createMetadata("gsbSyllabi", "enterprise_86433", metadata);
						}
					}

				} else {
					// For all alfresco mapping, don't update metadata
					/**
					 * Temporarily disable for testing on Syllabi 4/19/2018
					 */
					if(true) {
					System.out.println("Skipping existing file: "+fileId);
					return;
					}
					BoxFile file = new BoxFile(api, fileId);

					Metadata m = null;
					List<SecondaryType> aspects = doc.getSecondaryTypes();
					for (SecondaryType aspect : aspects) {
						if (aspect.getId().equals("P:gsb:casewriting")) {
							 m = file.getMetadata("gsbitops", "enterprise_86433");
						} else if (aspect.getId().equals("P:gsb:coursematerials")) {
							m = file.getMetadata("gsbSyllabi", "enterprise_86433");
						}
					}
					// m = file.getMetadata();

					if (!created) {
						System.out.println("NOT Newly created");
						if (m != null) {
							// Update existing metadata
							System.out.println("M Scope: " + m.getScope());
//							List<SecondaryType> aspects = doc.getSecondaryTypes();
							Metadata metadata = m;
							for (SecondaryType aspect : aspects) {
								if (aspect.getId().equals("P:gsb:casewriting")) {
									System.out.println(" " + aspect.getDisplayName() + " (" + aspect.getId() + ")");
									// metadata = replaceMetadata(caseMapping, doc,
									// m);
									metadata = getMetadata(caseMapping, doc, true, m);
									// file.createMetadata("gsbitops",
									// "enterprise_86433", metadata);
								} else if (aspect.getId().equals("P:gsb:coursematerials")) {
									System.out.println(" " + aspect.getDisplayName() + " (" + aspect.getId() + ")");
									// metadata = replaceMetadata(syllabiMapping,
									// doc, m);
									metadata = getMetadata(syllabiMapping, doc, true, m);
									// file.createMetadata("gsbSyllabi",
									// "enterprise_86433", metadata);
								}
							}
							file.updateMetadata(metadata);

						} else {
							// Add metadata to a file without any metadata
//							List<SecondaryType> aspects = doc.getSecondaryTypes();
							Metadata metadata = new Metadata();
							for (SecondaryType aspect : aspects) {
								if (aspect.getId().equals("P:gsb:casewriting")) {
									System.out.println(" " + aspect.getDisplayName() + " (" + aspect.getId() + ")");
									metadata = getMetadata(caseMapping, doc);
									file.createMetadata("gsbitops", "enterprise_86433", metadata);
								} else if (aspect.getId().equals("P:gsb:coursematerials")) {
									System.out.println(" " + aspect.getDisplayName() + " (" + aspect.getId() + ")");
									metadata = getMetadata(syllabiMapping, doc);
									file.createMetadata("gsbSyllabi", "enterprise_86433", metadata);
								}
							}
						}
					}
				}

				// create a shared link for syllabi in course listings
				// not testing currently, needs to be commented out for
				// non-syllabi
//				 createSharedLink(api, fileId);

				// get the file info for the mapping (need to remember why we
				// use a new
				// file)
				BoxFile tempFile = new BoxFile(api, fileId);
				BoxFile.Info fileInfo = tempFile.getInfo();
				// Add the mapping from Alfresco
				mapping.put((String) doc.getProperty("alfcmis:nodeRef").getFirstValue(), fileInfo.getName() + ","
						+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);

				// add the info for the cases only to a mapping
				BoxFolder.Info parentInfo = fileInfo.getParent();
				String parentName = parentInfo.getName();
				// System.out.println("ParentName: "+parentName);
				String fileName = fileInfo.getName();
				System.out.println("FileName: " + fileName);
				if (fileName.indexOf(".pdf") != -1) {
					String fileName2 = fileName.substring(0, fileName.indexOf(".pdf"));
					// System.out.println("FileName2: " + fileName2);
					// System.out.println("FileName2: " + fileName2.substring(0,
					// fileName2.length()-1));
					if (fileName2.equals(parentName)
							|| parentName.equals(fileName2.substring(0, fileName2.length() - 1))) {
						// System.out.println("Adding to mapping");
						mappingCases.put((String) doc.getProperty("alfcmis:nodeRef").getFirstValue(),
								fileInfo.getName() + "," + (String) doc.getProperty("alfcmis:nodeRef").getFirstValue()
										+ "," + fileId);
					}
				}

				return;
			} catch (BoxAPIException be) {
				System.out.println("BOX EXCEPTION(" + attempt + "): " + be.getResponse());
				attempt++;
				if(attempt >= 10) {
					mappingError.put(
							(String) doc.getProperty("alfcmis:nodeRef").getFirstValue(),
							doc.getProperty("cmis:name").getFirstValue() + ","
									+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + ","
									+ fileId);
					return;
				}
				try {
					Thread.sleep(1000 * attempt);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (CmisRuntimeException cre) {
				System.out.println("CMIS EXCEPTION: " + cre.getMessage());
				mappingError.put((String) doc.getProperty("alfcmis:nodeRef").getFirstValue(), null + ","
						+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);
				return;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mappingError.put((String) doc.getProperty("alfcmis:nodeRef").getFirstValue(), doc.getProperty("cmis:name").getFirstValue() + ","
						+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);
			}
		}
	}

	public static Metadata getMetadata(Map<String, BoxMetadata> mapping, Document doc) {
		return getMetadata(mapping, doc, false, null);
	}

	public static Metadata getMetadata(Map<String, BoxMetadata> mapping, Document doc, boolean replace, Metadata m) {
		Metadata metadata;
		boolean originalReplace = replace;
		if(replace) {
			metadata = m;
		} else {
			metadata = new Metadata();
		}
		
		for (String key : mapping.keySet()) {
			BoxMetadata bm = mapping.get(key);
			Property<Object> p = doc.getProperty(key);
//			System.out.println("--------------------------------------");
//			System.out.println("Object Name: " + doc.getName());
//
//			for (PropertyData<?> prop : doc.getProperties()) {
//				System.out.println(prop.getQueryName() + ": " + prop.getFirstValue());
//			}
//			System.out.println("--------------------------------------");
//			System.out.println("Property: "+p);
//			if(p.getFirstValue() != null) {
//				System.out.println("Property value: "+p.getFirstValue());
//			}
//			System.out.println(bm.getKey());
//			System.out.println(metadata.get(bm.getKey()));
			if(metadata.get(bm.getKey()) == null) {
				replace = false;
			} else {
				replace = originalReplace;
			}
			if (p != null && p.getFirstValue() != null) {
				// Handle values with a set of options
				if (bm.getValues() != null) {
					String[] vals = bm.getValues();
					if (Arrays.deepEquals(vals, bool)) {
						Object prop = p.getFirstValue();
						// Map the various boolean values to "Yes" or "No" in
						// box.
						if (prop instanceof Boolean) {
							// System.out.println("val "+((Boolean) prop ? "Yes"
							// : "No"));
							if(replace) {
								metadata.replace(bm.getKey(), (Boolean) prop ? "Yes" : "No");
							} else {
								metadata.add(bm.getKey(), (Boolean) prop ? "Yes" : "No");
							}
							// metadata.add(bm.getKey()+"1", (Boolean) prop ?
							// "Yes" : "No");
						} else if (prop instanceof String) {
							if(replace) {
								metadata.replace(bm.getKey(), ((String) prop).equals("Yes") ? "Yes" : "No");
							} else {
								metadata.add(bm.getKey(), ((String) prop).equals("Yes") ? "Yes" : "No");
							}
							// metadata.add(bm.getKey()+"1", ((String)
							// prop).equals("Yes") ? "Yes" : "No");
						} else {
							System.out.println("NEED to handle prop: " + p.getDisplayName());
						}
					} else {
						String val = (String) p.getFirstValue();
						for (String s : vals) {
							if (s.toLowerCase().equals(val.toLowerCase())) {
								if(replace) {
									metadata.replace(bm.getKey(), s);
								} else {
									metadata.add(bm.getKey(), s);
								}
							}
						}
					}
				} else {
					if (p.getFirstValue() instanceof String && !((String) p.getFirstValue()).trim().equals("")) {
						// if(key.equals("gsb:cwoAcademicArea") &&
						// p.getFirstValue().equals("Political Economy")) {
						// metadata.add(bm.getKey(), "Political Economics");
						// Map the Business Topics "Energy", "Global Business",
						// and "Public Sector" to Other Areas with the same
						// values
						if (key.equals("gsb:cwoBusinessTopics") && p.getFirstValue().equals("Energy")) {
							if(metadata.get("/otherAreas") == null) {
								replace = false;
							} else {
								replace = originalReplace;
							}
							if(replace) {
								metadata.replace("/otherAreas", "Energy");
							} else {
								metadata.add("/otherAreas", "Energy");
							}
						} else if (key.equals("gsb:cwoBusinessTopics") && p.getFirstValue().equals("Global Business")) {
							if(metadata.get("/otherAreas") == null) {
								replace = false;
							} else {
								replace = originalReplace;
							}
							if(replace) {
								metadata.replace("/otherAreas", "Global Business");
							} else {
								metadata.add("/otherAreas", "Global Business");
							}
						} else if (key.equals("gsb:cwoBusinessTopics") && p.getFirstValue().equals("Social Innovation")) {
							if(metadata.get("/otherAreas") == null) {
								replace = false;
							} else {
								replace = originalReplace;
							}
							if(replace) {
								metadata.replace("/otherAreas", "Social Innovation");
							} else {
								metadata.add("/otherAreas", "Social Innovation");
							}
						} else if (key.equals("gsb:cwoBusinessTopics") && p.getFirstValue().equals("Public Sector")) {
							if(metadata.get("/otherAreas") == null) {
								replace = false;
							} else {
								replace = originalReplace;
							}
							if(replace) {
								metadata.replace("/otherAreas", "Public Sector");
							} else {
								metadata.add("/otherAreas", "Public Sector");
							}
							// Map HBP Discipline to use uppercase for each word
						} else if (key.equals("gsb:cwoHbpDiscipline") && p.getFirstValue().equals("Business this")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Business Ethics");
							} else {
								metadata.add(bm.getKey(), "Business Ethics");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline")
								&& p.getFirstValue().equals("Business & government relations")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Business & Government Relations");	
							} else {
								metadata.add(bm.getKey(), "Business & Government Relations");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline")
								&& p.getFirstValue().equals("Information technology")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Information Technology");
							} else {
								metadata.add(bm.getKey(), "Information Technology");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline")
								&& p.getFirstValue().equals("International business")) {
							if(replace) {
								metadata.replace(bm.getKey(), "International Business");
							} else {
								metadata.add(bm.getKey(), "International Business");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline")
								&& p.getFirstValue().equals("Human resources management")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Human Resource Management");
							} else {
								metadata.add(bm.getKey(), "Human Resource Management");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline")
								&& p.getFirstValue().equals("Operations management")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Operations Management");
							} else {
								metadata.add(bm.getKey(), "Operations Management");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline")
								&& p.getFirstValue().equals("Organizational behavior")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Organizational Behavior");
							} else {
								metadata.add(bm.getKey(), "Organizational Behavior");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline") && p.getFirstValue().equals("Service management")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Service Management");
							} else {
								metadata.add(bm.getKey(), "Service Management");
							}
						} else if (key.equals("gsb:cwoHbpDiscipline") && p.getFirstValue().equals("Social enterprise")) {
							if(replace) {
								metadata.replace(bm.getKey(), "Social Enterprise");
							} else {
								metadata.add(bm.getKey(), "Social Enterprise");
							}
						} else {
							if(replace) {
								metadata.replace(bm.getKey(), (String) p.getFirstValue());
							} else {
								metadata.add(bm.getKey(), (String) p.getFirstValue());
							}
						}
					} else if (p.getFirstValue() instanceof Calendar) {
						Calendar c = (Calendar) p.getFirstValue();
						Date d = c.getTime();
//						String out = DateFormat.getDateInstance(DateFormat.FULL).format(d);
						// SimpleDateFormat ISO8601DATEFORMAT = new
						// SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
						SimpleDateFormat RFC3339DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
						// System.out.println(RFC3339DATEFORMAT.format(d));
						// System.out.println("Date: "+p.getFirstValue());
						// System.out.println("Date: "+out);
						if(replace) {
							metadata.replace(bm.getKey(), RFC3339DATEFORMAT.format(d));
						} else {
							metadata.add(bm.getKey(), RFC3339DATEFORMAT.format(d));
						}
					} else {
						System.out.println("Unknown type: " + bm.getKey() + " " + p.getFirstValue());
					}
				}
			} else {
				// Null valued cwoAccess should be "Not for Distribution"
				if (key.equals("gsb:cwoAccess")
						&& (p == null || p.getFirstValue() == null || ((String) p.getFirstValue()).trim().equals(""))) {
					if(replace) {
						metadata.replace(bm.getKey(), "Not for Distribution");
					} else {
						metadata.add(bm.getKey(), "Not for Distribution");
					}
//					System.out.println("Setting default value for cwoAccess");
				}
				if (p == null) {

				} else if (p.getFirstValue() == null) {

				}
				// System.out.println("Unexpected error. Property '"+key+"' ");
			}
		}
		// tags
		List<String> tags = getTags(doc.getId());
		if (tags != null && tags.size() > 0) {
			String tagStr = "";
			Iterator<String> i = tags.iterator();
			while (i.hasNext()) {
				tagStr += i.next();
				if (i.hasNext()) {
					tagStr += ", ";
				}
			}
			if(replace) {
				metadata.replace("/tags", tagStr);
			} else {
				metadata.add("/tags", tagStr);
			}
		}
		return metadata;
	}

	public static void printTemplateInfo(BoxAPIConnection api) {
		Iterable<MetadataTemplate> templates = MetadataTemplate.getEnterpriseMetadataTemplates(api);
		for (MetadataTemplate templateInfo : templates) {
			// Do something with the metadata template.
			System.out.println("Template Display Name: " + templateInfo.getDisplayName());
			System.out.println("Template Key: " + templateInfo.getTemplateKey());
			System.out.println("Template Scope: " + templateInfo.getScope());
			for (Field field : templateInfo.getFields()) {
				System.out.println("Field Key: " + field.getKey() + "(" + field.getType() + ")");
			}
		}
	}

	public static void box(String boxKey) {
		try {
			System.out.println("Hello World!");
			// Developer Token Auth
			// BoxAPIConnection api = new BoxAPIConnection(boxKey);
			// Oauth Auth
			BoxAPIConnection api = new BoxAPIConnection("opuitnahtbvfefalzhzav9b9e2k1jnbr",
					"GET5iaqeYIop7ETZSuu3tj6lz3zulMJK", "sjy4G3oJv8w02JORXaEI33dZU1FZg6JI",
					"ljGsC1eNWyTStbv3nkKB1vz6C0ixTOIyOBUZRJjuYBLM31zOpoSJGK5wdi7hAirx");
			api.setAutoRefresh(true);
			BoxFolder rootFolder = BoxFolder.getRootFolder(api);
			for (BoxItem.Info itemInfo : rootFolder) {
				System.out.format("[%s] %s\n", itemInfo.getID(), itemInfo.getName());
			}
			BoxFolder test = new BoxFolder(api, "27063021327");
			BoxFolder.Info info = test.getInfo();
			System.out.println("Info: " + info.getID());
			for (BoxItem.Info itemInfo : test) {
				System.out.format("[%s] %s\n", itemInfo.getID(), itemInfo.getName());
			}

			FileInputStream stream;
			try {

				// MetadataTemplate templateInfo =
				// MetadataTemplate.getMetadataTemplate(api, "gsbitops");
				Iterable<MetadataTemplate> templates = MetadataTemplate.getEnterpriseMetadataTemplates(api);
				// MetadataTemplate templateInfo =
				// MetadataTemplate.getMetadataTemplate(api);
				for (MetadataTemplate templateInfo : templates) {
					// Do something with the metadata template.
					System.out.println("Template: " + templateInfo.getDisplayName());
					System.out.println("Template: " + templateInfo.getTemplateKey());
					System.out.println("Template: " + templateInfo.getScope());
					for (Field field : templateInfo.getFields()) {
						System.out.println("Field: " + field.getKey());
						System.out.println("Field: " + field.getType());
					}
				}

				stream = new FileInputStream(BOX_AUTH_FILE_PATH+"test.txt");

				BoxFile.Info newFileInfo = test.uploadFile(stream, "test.txt");
				stream.close();
				System.out.println("Info: " + newFileInfo.getID());

				BoxFile file = new BoxFile(api, newFileInfo.getID());
				System.out.println("id: " + file.getID());
				file.createMetadata("gsbitops", "enterprise_86433",
						new Metadata().add("/title", "test").add("/productType", "Case"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (BoxAPIException be) {
			System.out.println(be.getResponse());
			System.out.println(be.getResponseCode());
			be.printStackTrace();
		}
	}

	/**
	 * Recursively get a box file id given a path and file name.
	 * 
	 * @param api
	 * @param path
	 * @param name
	 * @param folder
	 * @return
	 */
	
	public static String findBoxFileId(BoxAPIConnection api, String path, String name, BoxFolder folder) {
		int attempt = 1;
		while (true) {
		try {
//		 System.out.println("--------------------------------------");
//		 System.out.println("findBoxFileId");
//		 System.out.println("--------------------------------------");
//		 System.out.println("Path: "+path);
//		 System.out.println("File: "+name);
		String fileId = null;
		String delim = "/";
		if (folder == null) {
			folder = BoxFolder.getRootFolder(api);
		}
//		 System.out.println("Folder: "+folder.getID());

//		 for (BoxItem.Info itemInfo : folder) {
//		 System.out.format("[%s] %s\n", itemInfo.getID(), itemInfo.getName());
//		 }

		if (path != null && path.length() >= 1) {
			int index = path.indexOf(delim);
			String nFolder = path;
			if (index != -1) {
				nFolder = path.substring(0, index);
			}
			 System.out.println("nFolder: "+nFolder);
			Iterable<BoxItem.Info> info = folder.getChildren();
			for (BoxItem.Info i : info) {
				String bName = i.getName();
				 System.out.println("bName: "+bName);
				if (bName.equals(nFolder)) {
					if (path.length() > index + 1) {
						BoxFolder bFolder = new BoxFolder(api, i.getID());
						return findBoxFileId(api, index == -1 ? "" : path.substring(index + 1), name, bFolder);
					}

				}
			}
		} else {
			Iterable<BoxItem.Info> info = folder.getChildren();
			for (BoxItem.Info i : info) {
				String bName = i.getName();
				if (bName.equals(name)) {
					 System.out.println("File ID: " + i.getID() + " for " + name);
					return i.getID();
				}
			}
		}

		return fileId;
		} catch (BoxAPIException be) {
			System.out.println("BOX EXCEPTION: " + be.getResponse());
			attempt++;
			try {
				Thread.sleep(1000*attempt);
//				findBoxFileId(api, path, name, folder);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		} 
	}

	/**
	 * Recursively get a box folder id given a path and file name.
	 * 
	 * @param api
	 * @param path
	 * @param name
	 * @param folder
	 * @return
	 */
	public static String findBoxFolderId(BoxAPIConnection api, String path, BoxFolder folder, Node currentNode) {
		// System.out.println("--------------------------------------");
		// System.out.println("findBoxFolderId");
		// System.out.println("--------------------------------------");
		// System.out.println("Path: " + path);
		String folderId = null;
		String delim = "/";
		if (folder == null) {
			folder = BoxFolder.getRootFolder(api);
			// BoxNode root = new BoxNode("root", "0");
			if (rootNode == null) {
				currentNode = rootNode = new Node("", "0", folder);
			} else {
				currentNode = rootNode;
			}
		}
		//
		// System.out.println("Folder: "+folder.getID());
		// for (BoxItem.Info itemInfo : folder) {
		// System.out.format("[%s] %s\n", itemInfo.getID(), itemInfo.getName());
		// }
		//
		Node folderNode = currentNode.findNode(path);
		// System.out.println("CurrentNode: "+currentNode.getId());
		// Node n = rootNode.findNode(path);
		if (folderNode != null) {
			// System.out.println("folderNode: "+folderNode.getId());
			return folderNode.getId();
		} else if (path != null && path.length() >= 1) {
			// System.out.println("Path: "+path);
			int index = path.indexOf(delim);
			String nFolder = path;
			if (index != -1) {
				nFolder = path.substring(0, index);
			}
			// System.out.println("nFolder: " + nFolder);
			Iterable<BoxItem.Info> info = folder.getChildren();
			// Node n = currentNode.findNode(nFolder);
			for (BoxItem.Info i : info) {
				String bName = i.getName();

				Node n = currentNode.findNode(bName);
				if (n == null) {
					n = new Node(bName, i.getID(), null);
					currentNode.addNode(n);
				}

				// System.out.println("bName: " + bName);
				if (bName.equals(nFolder)) {
					if (path.length() > index + 1) {
						BoxFolder bFolder = new BoxFolder(api, i.getID());
						if (index == -1) {
							return i.getID();
						} else {
							return findBoxFolderId(api, path.substring(index + 1), bFolder, n);
						}
					}
				}
			}
			// folder doesn't exist yet, create it
			if (folderId == null) {
				BoxItem.Info i = folder.createFolder(nFolder);

				Node n = new Node(nFolder, i.getID(), folder);
				currentNode.addNode(n);

				// System.out.println("Created folder "+i.getName());
				// We might need to create more than one folder if the hierarchy
				// doesn't exist yet
				if (path.length() > index + 1) {
					BoxFolder bFolder = new BoxFolder(api, i.getID());
					if (index == -1) {
						return i.getID();
					} else {
						return findBoxFolderId(api, path.substring(index + 1), bFolder, n);
					}
				}
				return i.getID();
			}
		}

		return folderId;
	}

	public static BoxItem.Info findInBox(String name, BoxSearch boxSearch) {
		// BoxSearch boxSearch = new BoxSearch(api);
		BoxSearchParameters bsp = new BoxSearchParameters();
		// System.out.println("searching for "+name);
		bsp.setQuery(name);
		BoxMetadataFilter bmf = new BoxMetadataFilter();
		bmf.setScope("enterprise");
		bsp.setMetadataFilter(bmf);

		PartialCollection<BoxItem.Info> searchResults = null;
		// Starting point of the result set
		long offset = 0;
		// Number of results that would be pulled back
		long limit = 1;

		// Storing the full size of the results
		long fullSizeOfResult = 0;

		while (offset <= fullSizeOfResult) {
			searchResults = boxSearch.searchRange(offset, limit, bsp);
			fullSizeOfResult = searchResults.fullSize();

			System.out.println("offset: " + offset + " of fullSizeOfResult: " + fullSizeOfResult);
			for (BoxItem.Info itemInfo : searchResults) {
				System.out.println("File Found: " + itemInfo.getName() + ", Owner: " + itemInfo.getOwnedBy().getID());
			}

			offset += limit;
		}
		return searchResults.iterator().next();
	}
	
	
	
	
	public static void getDocs(Session session, AlfrescoFolder folder, BoxAPIConnection api, String boxPath) {
		System.out.println("--------------------------------------");
		// Iterate over all the folders
		ItemIterable<QueryResult> subFolders = getFolders(session, folder);
		for (QueryResult result : subFolders) {
			CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId")
					.getFirstValue()));
			getDocs(session, (AlfrescoFolder) o, api, boxPath + "/"
					+ result.getPropertyById("cmis:name").getFirstValue());
		}

		// When no more folders, get all documents
		ItemIterable<QueryResult> files = getFiles(session, folder);
		String folderId = null;
		folderId = findBoxFolderId(api, boxPath, null, null);
		int page = 1;
		do {
			System.out.println("Items: " + files.getPageNumItems());
			System.out.println("Total: " + files.getTotalNumItems());
			if (files.getHasMoreItems()) {
				System.out.println("More Items");
				System.out.println("Items/page: " + ITEMS_PER_PAGE);
				System.out.println("Page: " + page);
				files = files.skipTo(ITEMS_PER_PAGE * page).getPage();
			}
			int item = 1;
			for (QueryResult result : files) {
				//System.out.println("CMIS Object Id: ["+result.getPropertyById("cmis:objectId").getFirstValue()+"]");
				CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId").getFirstValue()));

				if (!o.getProperty("cmis:name").getFirstValue().equals("SM 232 Video TRT 19min52sec.mp4")
						&& !o.getProperty("cmis:name").getFirstValue().equals("POLECON332_F01-02_SYL_Savage.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("GSBGEN380_ SP12-13_SYL_Lietz.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("OB661_SP99-00_SYL_Jost.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("SM278 - Interview with Peter Terium.mp4")) {
					// TODO move try catch to here for box api exceptions
					existsInBox(api, folderId, (Document) o, boxPath);

				} 
				item++;
			}
			page++;
		} while (files.getHasMoreItems());
	}
	
	public static void existsInBox(BoxAPIConnection api, String folderId, Document doc, String path) {
		System.out.println("Document: "+doc.getName());
		String title = (String) doc.getProperty("cmis:name").getFirstValue();
		// Need to add file extension (based on mimetype) for files without an extension
		// there is no one to one mapping for mimetype to extension, for now just add .pdf
		if((title.length() > 4 && !(title.charAt(title.length()-4) == '.')) || !title.contains(".")) {
			String mimetype = (String) doc.getProperty("cmis:contentStreamMimeType").getFirstValue();
			if ("application/pdf".equals(mimetype) || "application/octet-stream".equals(mimetype)) {
				title = title + ".pdf";
			}
		}	
		
		// get the folder to put the file in
		BoxFolder folder = new BoxFolder(api, folderId);
		// check to see if file exists
		String fileId = findBoxFileId(api, "", title, folder);
		if (fileId == null) {
			if (doc.getContentStream() == null) {
				// For working (research) papers only ? add a placeholder pdf
				// FileInputStream is = new
				// FileInputStream(BOX_AUTH_FILE_PATH+"placeholder.pdf");
			} else {
				BigInteger length = (BigInteger) doc.getProperty("cmis:contentStreamLength").getFirstValue();
				System.out.println("File length: " + length);
				long docSize = length.longValue();
				if (docSize > MAX_FILE_SIZE) {
					mappingError.put(
							(String) doc.getProperty("alfcmis:nodeRef").getFirstValue(),
							doc.getProperty("cmis:name").getFirstValue() + ","
									+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);
					return;
				}
			}
		} else {
			mapping.put((String) doc.getProperty("alfcmis:nodeRef").getFirstValue(), doc.getProperty("cmis:name").getFirstValue() + ","
					+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);
		}
		

	}

}
