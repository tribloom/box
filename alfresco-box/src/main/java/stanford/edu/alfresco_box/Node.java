package stanford.edu.alfresco_box;

import java.util.ArrayList;
import java.util.List;

import com.box.sdk.BoxItem;

public class Node {
	private String name;
	private String id;
	private BoxItem item;
	

	private Node parent;
	private List<Node> children;

	public Node(String name, String id, BoxItem item) {
		this.name = name;
		this.id = id;
		this.item = item;
		children = new ArrayList<Node>();
	}

	public void addNode(Node child) {
		children.add(child);
		child.parent = this;
	}

	public List<Node> getChildren() {
		return children;
	}

	public Node findNode(String path) {
		// Box path does not begin with /
		// i.e. "GSB BOX (Production)/Case Writing/E/E601-E650/E622"
		if (name.equals(path)) {
			//System.out.println("Hit: "+path);
			return this;
		}
		String delim = "/";
		int index = path.indexOf(delim);
		if (index != -1) {
			String folderName = path.substring(0, index);
			if(this.name.equals(folderName)) {
				for(Node n : children) {
					Node node = n.findNode(path.substring(index + 1));
					if(node != null) {
						return node;
					}
				}
			}
		} else {
			for(Node n : children) {
				Node node = n.findNode(path);
				if(node != null) {
					return node;
				}
			}
		}
		return null;
	}

	public Node getRoot() {
		if (parent == null) {
			return this;
		} else {
			return parent.getRoot();
		}
	}

	public String getId() {
		return id;
	}
	public BoxItem getItem() {
		return item;
	}

}