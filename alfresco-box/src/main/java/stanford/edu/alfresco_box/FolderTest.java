package stanford.edu.alfresco_box;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.ObjectIdImpl;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.lang3.SystemUtils;

import com.box.sdk.BoxAPIConnection;

public class FolderTest {
	private static String ALFRESCO_URL = "http://54.202.234.8:8080";
	private static String boxClientId = "opuitnahtbvfefalzhzav9b9e2k1jnbr";
	private static String boxClientSecret = "GET5iaqeYIop7ETZSuu3tj6lz3zulMJK";
	private static final int ITEMS_PER_PAGE = 100;

	
	public static void main(String[] args) throws Exception {
		String user = "admin-tribloom";
		String password = "tr7b00n";
		String url = "http://localhost:8080/alfresco/api/-default-/public/cmis/versions/1.1/atom";
		String repositoryId = "-default-";
		String docPath = "";
		String boxKey = "";
		String boxFolder = "";
		if (args.length > 0) {
			user = args[0];
		}
		if (args.length > 1) {
			password = args[1];
		}
		if (args.length > 2) {
			url = args[2];
		}
		if (args.length > 3) {
			repositoryId = args[3];
		}
		if (args.length > 4) {
			docPath = args[4];
		}
		if (args.length > 5) {
			boxKey = args[5];
		}
		if (args.length > 6) {
			boxFolder = args[6];
		}
		System.out.println("Connecting to " + url + " with user '" + user + "' using password '" + password + "'");
		System.out.println("Repository ID: " + repositoryId);
		System.out.println("Doc Path: " + docPath);

		// Exit if no document path
		if (docPath == null || "".equals(docPath)) {
			return;
		}

		// get a CMIS session
		Session session = connectCMIS(user, password, url, repositoryId);
		
		// Get the base folder given by the path
		AlfrescoFolder f = (AlfrescoFolder) session.getObjectByPath(docPath);
		
		moveToBox(session, f);
	}
	
	public static Session connectCMIS(String user, String password, String url, String repositoryId) {
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();
		// parameter.put(SessionParameter.ATOMPUB_URL, url);
		// parameter.put(SessionParameter.BINDING_TYPE,
		// BindingType.ATOMPUB.value());
		parameter.put(SessionParameter.BROWSER_URL, url);
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.BROWSER.value());
		parameter.put(SessionParameter.AUTH_HTTP_BASIC, "true");
		parameter.put(SessionParameter.USER, user);
		parameter.put(SessionParameter.PASSWORD, password);
		parameter.put(SessionParameter.REPOSITORY_ID, repositoryId);
		parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");
		List<Repository> repositories = factory.getRepositories(parameter);
		Session session = repositories.get(0).createSession();
		System.out.println("Connected to repository:" + repositories.get(0).getId());
		return session;
	}
	
	public static void moveToBox(Session session, AlfrescoFolder folder) {
		// Get all folders under the given path
		ItemIterable<QueryResult> subFolders = getFolders(session, folder);
		int page = 0;
		do {
			if (subFolders.getPageNumItems() > 0) {
				if (subFolders.getHasMoreItems() && page > 0) {
					subFolders = subFolders.skipTo(ITEMS_PER_PAGE * page).getPage();
					System.out.println(folder.getPath()+", "+subFolders.getTotalNumItems());
					//System.out.println("Folder Page: " + page);
				}
				for (QueryResult result : subFolders) {
					CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId")
							.getFirstValue()));
					moveToBox(session, (AlfrescoFolder) o);
				}
				page++;
			}
		} while (subFolders.getHasMoreItems());
	}
	
	public static ItemIterable<QueryResult> getFolders(Session session, AlfrescoFolder f) {
		// Get all folders under that folder
		String cql = "SELECT * FROM cmis:folder WHERE IN_FOLDER('" + f.getId() + "')";
		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(ITEMS_PER_PAGE);
		ItemIterable<QueryResult> results = session.query(cql, false);
		return results;
	}
	
	public static ItemIterable<QueryResult> getFiles(Session session, AlfrescoFolder f) {
		// Get all documents under that folder
		String cql = "SELECT * FROM cmis:document WHERE IN_FOLDER('" + f.getId() + "')";
		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(ITEMS_PER_PAGE);

		ItemIterable<QueryResult> results = session.query(cql, false, oc);
		return results;
	}

}
