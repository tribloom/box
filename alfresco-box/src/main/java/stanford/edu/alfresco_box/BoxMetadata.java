package stanford.edu.alfresco_box;

public class BoxMetadata {
	private String key;
	private String[] values;
	
	public static boolean checkString(String key) {
		if(key != null && key.trim().length() > 0) {
			return true;
		}
		return false;
	}
	
	public BoxMetadata(String key) throws Exception {
		if(checkString(key)) {
			this.key = key;
		} else {
			throw new Exception("Key must contain a valid non-empty, non-null value.");
		}
	}
	
	public BoxMetadata(String key, String[] values) throws Exception {
		if(!checkString(key)) {
			throw new Exception("Key must contain a valid non-empty, non-null value.");
		}
		for(String val : values) {
			if(!checkString(val)) {
				throw new Exception("Values must contain valid non-empty, non-null values.");
			}
		}
		this.key = key;
		this.values = values;
	}
	
	public String getKey() {
		return new String(key);
	}
	public String[] getValues() {
		if (values != null) {
			String[] vals = new String[values.length];
			for (int i = 0; i < values.length; i++) {
				vals[i] = values[i];
			}
			return vals;
		} else {
			return null;
		}
	}

}
