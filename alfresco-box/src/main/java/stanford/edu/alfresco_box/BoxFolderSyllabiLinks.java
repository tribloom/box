package stanford.edu.alfresco_box;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIConnectionListener;
import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxSharedLink;

public class BoxFolderSyllabiLinks {
	public static String FILE_PATH = "C:\\Users\\Michael\\Downloads\\";
	public static String FILE_NAME_S = "Spring15.csv";
	public static String FILE_OUT = "Spring15 (3).csv";
	public static int FILENAME_INDEX = 2;
	public static String FILE_NAME_B = "2018-02-12-SyallabiinCourseListings-mapping.csv";
	public static String BOX_URL = "https://stanford.app.box.com/file/";
	public static String[] HEADER = {"Quarter","Class Code","Subject","Course #","Section #","Course Title","Prof's Last Name","File Name","Link","Box Id", "Box Link","Video Code","Video Link"};
	private static String boxClientId = "opuitnahtbvfefalzhzav9b9e2k1jnbr";
	private static String boxClientSecret = "GET5iaqeYIop7ETZSuu3tj6lz3zulMJK";


	public static void main(String[] args) throws IOException {
		CSVParser pS = readCSV(FILE_PATH + FILE_NAME_S);
		CSVParser pB = readCSV(FILE_PATH + FILE_NAME_B);

		List<CSVRecord> recordsS = pS.getRecords();
		System.out.println("Syllabi CSV\n");
		printRecords(recordsS);
		List<CSVRecord> recordsB = pB.getRecords();
		System.out.println("Box CSV\n");
		printRecords(recordsB);

		List<String[]> out = addBoxUrl(recordsS, recordsB);

		FileWriter fw = new FileWriter(FILE_PATH + FILE_OUT);
		
		CSVPrinter printer = new CSVPrinter(fw, CSVFormat.EXCEL);
		printer.printRecord(HEADER);
		for(String[] s : out) {
			printer.printRecord(s);
		}
		printer.close();

	}

	public static List<String[]> addBoxUrl(List<CSVRecord> to, List<CSVRecord> from) {
		List<String[]> retVal = new ArrayList<String[]>();
		boolean first = true;
		for (CSVRecord r : to) {
			if (!first) {
				System.out.println(r);
				String fileName = r.get(FILENAME_INDEX);
				System.out.println("FileName: " + fileName);
				String[] row = new String[13];
				CSVRecord br = getBoxRecord(fileName, from);
				System.out.println(br);
				for (int i = 0; i < r.size(); i++) {
//					if (i == 8) {
//						row[i] = "=\""+r.get(i).substring(0, r.get(i).lastIndexOf("/"))+"\"&H2";
//					}
					if (i == 9 ) {
						// Insert fileId
						if (br != null) {
							row[i] = br.get(2);
						}
						row[11] = r.get(i);
						i++;
					} 
					if (i == 6 ) {
						// insert box link
						if (br != null) {
//							row[i] = BOX_URL + br.get(2);
							row[i] = getBoxLink(br.get(2));
						}
						row[7] = r.get(i);
						i++;
					}
					if (i < 9) {
						row[i] = r.get(i);
					}
				}
				retVal.add(row);
			}
			first = false;
		}
		return retVal;
	}

	public static CSVRecord getBoxRecord(String fileName, List<CSVRecord> records) {
		if (fileName != null) {
			for (CSVRecord r : records) {
				if (fileName.equals(r.get(0))) {
					return r;
				}
			}
		}
		return null;
	}

	public static void printRecords(List<CSVRecord> records) {
		for (CSVRecord r : records) {
			System.out.println(r);
		}
	}

	public static CSVParser readCSV(String fileName) {
		try {
			File file = new File(fileName);
			CSVParser parser = CSVParser.parse(file, Charset.defaultCharset(), CSVFormat.EXCEL);
			return parser;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static String getBoxLink(String fileId) {
		final App a = new App();
		a.readAuthInfo();
		BoxAPIConnection api = new BoxAPIConnection(boxClientId, boxClientSecret, a.accessToken, a.refreshToken);
		api.addListener(new BoxAPIConnectionListener() {
			public void onRefresh(BoxAPIConnection api) {
				String newAccessToken = api.getAccessToken();
				String newrefreshToken = api.getRefreshToken();
				System.out.println("AccessToken: " + newAccessToken);
				System.out.println("RefreshToken: " + newrefreshToken);
				// a = new App();
				a.accessToken = newAccessToken;
				a.refreshToken = newrefreshToken;
				a.writeAuthInfo();
				// update new access and refresh token in DB/property
			}

			public void onError(BoxAPIConnection api, BoxAPIException error) {
				System.out.println("Error in Box account details. " + error.getMessage());
			}
		});
		System.out.println("Completed Box authentication");
		
	    BoxFile file = new BoxFile(api, fileId);
	    BoxFile.Info info = file.getInfo();
	    BoxSharedLink link = info.getSharedLink();
	    System.out.println("Shared Link 2: "+link.getURL());
	    return link.getURL();

	}
}
