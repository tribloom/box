/**
 * 
 */
package stanford.edu.alfresco_box;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.ObjectIdImpl;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.commons.lang3.SystemUtils;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIConnectionListener;
import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;

/**
 * @author Michael
 *
 */
public class BoxMigration {
	private static String boxClientId = "opuitnahtbvfefalzhzav9b9e2k1jnbr";
	private static String boxClientSecret = "GET5iaqeYIop7ETZSuu3tj6lz3zulMJK";
	private static Map<String, String> mapping = new HashMap<String, String>();
//	private static Map<String, String> mappingCases = new HashMap<String, String>();
	private static Map<String, String> mappingError = new HashMap<String, String>();
	private static final String BOX_AUTH_FILE = "boxAuth.txt";
	private static String BOX_AUTH_FILE_PATH;
	private static final String BOX_AUTH_FILE_PATH_LINUX = "/home/ec2-user/";
	private static final String BOX_AUTH_FILE_PATH_WIN = "C:\\Users\\Michael\\Downloads\\";
	private static final int ITEMS_PER_PAGE = 100;
	private static final long MAX_FILE_SIZE = 5000000000L; // bytes of 5 GB





	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String user = "admin-tribloom";
		String password = "tr7b00n";
		String url = "http://gsb-content.stanford.edu:8080/alfresco/api/-default-/public/cmis/versions/1.1/browser";
		String repositoryId = "-default-";
		String docPath = "";
		String boxKey = "3VerZhkgLILbO7d5ybuNas8K359KFIbk";
		String boxFolder = "";
		if (args.length > 0) {
			docPath = args[0];
		}
		if (args.length > 1) {
			boxFolder = args[1];
		} else {
			boxFolder = "GSB BOX (Production)"+docPath;
		}
		if (docPath == null || "".equals(docPath)) {
			return;
		}
		
		String os = System.getProperty("os.name");
		Boolean isWin = SystemUtils.IS_OS_WINDOWS;
		System.out.println("Operating System: "+os+" is windows? "+isWin);
		if(isWin) {
			BOX_AUTH_FILE_PATH = BOX_AUTH_FILE_PATH_WIN;
		} else {
			BOX_AUTH_FILE_PATH = BOX_AUTH_FILE_PATH_LINUX;
		}

		System.out.println("Connecting to " + url + " with user '" + user + "' using password '" + password + "'");
		//System.out.println("Repository ID: " + repositoryId);
		System.out.println("Moving " + docPath + " to "+boxFolder);
		
		// get a CMIS session
		Session session = connectCMIS(user, password, url, repositoryId);
		
		// Get the base folder given by the path
		AlfrescoFolder alfFolder = (AlfrescoFolder) session.getObjectByPath(docPath);
		
		BoxAPIConnection api = connectBox();

		getDocs(session, alfFolder, api, boxFolder);
	}
	
	public static void getDocs(Session session, AlfrescoFolder folder, BoxAPIConnection api, String boxPath) {
		System.out.println("--------------------------------------");
		// Iterate over all the folders
		ItemIterable<QueryResult> subFolders = getFolders(session, folder);
		for (QueryResult result : subFolders) {
			CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId")
					.getFirstValue()));
			getDocs(session, (AlfrescoFolder) o, api, boxPath + "/"
					+ result.getPropertyById("cmis:name").getFirstValue());
		}

		// When no more folders, get all documents
		ItemIterable<QueryResult> files = getFiles(session, folder);
		String folderId = null;
		folderId = findBoxFolderId(api, boxPath, null, null);
		int page = 1;
		do {
			System.out.println("Items: " + files.getPageNumItems());
			System.out.println("Total: " + files.getTotalNumItems());
			if (files.getHasMoreItems()) {
				System.out.println("More Items");
				System.out.println("Items/page: " + ITEMS_PER_PAGE);
				System.out.println("Page: " + page);
				files = files.skipTo(ITEMS_PER_PAGE * page).getPage();
			}
			int item = 1;
			for (QueryResult result : files) {
				System.out.println("CMIS Object Id: ["+result.getPropertyById("cmis:objectId").getFirstValue()+"]");
				CmisObject o = session.getObject(new ObjectIdImpl((String) result.getPropertyById("cmis:objectId").getFirstValue()));

				System.out.println(item + ": Transfer to " + folderId + " document "
						+ o.getProperty("cmis:name").getFirstValue());
				if (!o.getProperty("cmis:name").getFirstValue().equals("SM 232 Video TRT 19min52sec.mp4")
						&& !o.getProperty("cmis:name").getFirstValue().equals("POLECON332_F01-02_SYL_Savage.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("GSBGEN380_ SP12-13_SYL_Lietz.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("OB661_SP99-00_SYL_Jost.pdf")
						&& !o.getProperty("cmis:name").getFirstValue().equals("SM278 - Interview with Peter Terium.mp4")) {
					// TODO move try catch to here for box api exceptions
					existsInBox(api, folderId, (Document) o, boxPath);

				} 
				item++;
			}
			page++;
		} while (files.getHasMoreItems());
	}
	
	public static void existsInBox(BoxAPIConnection api, String folderId, Document doc, String path) {
		System.out.println("Document: "+doc.getName());
		String title = (String) doc.getProperty("cmis:name").getFirstValue();
		// Need to add file extension (based on mimetype) for files without an extension
		// there is no one to one mapping for mimetype to extension, for now just add .pdf
		if((title.length() > 4 && !(title.charAt(title.length()-4) == '.')) || !title.contains(".")) {
			String mimetype = (String) doc.getProperty("cmis:contentStreamMimeType").getFirstValue();
			if ("application/pdf".equals(mimetype) || "application/octet-stream".equals(mimetype)) {
				title = title + ".pdf";
			}
		}	
		
		// get the folder to put the file in
		BoxFolder folder = new BoxFolder(api, folderId);
		// check to see if file exists
		String fileId = findBoxFileId(api, "", title, folder);
		if (fileId == null) {
			if (doc.getContentStream() == null) {
				// For working (research) papers only ? add a placeholder pdf
				// FileInputStream is = new
				// FileInputStream(BOX_AUTH_FILE_PATH+"placeholder.pdf");
			} else {
				BigInteger length = (BigInteger) doc.getProperty("cmis:contentStreamLength").getFirstValue();
				System.out.println("File length: " + length);
				long docSize = length.longValue();
				if (docSize > MAX_FILE_SIZE) {
					mappingError.put(
							(String) doc.getProperty("alfcmis:nodeRef").getFirstValue(),
							doc.getProperty("cmis:name").getFirstValue() + ","
									+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);
					return;
				}
			}
		} else {
			mapping.put((String) doc.getProperty("alfcmis:nodeRef").getFirstValue(), doc.getProperty("cmis:name").getFirstValue() + ","
					+ (String) doc.getProperty("alfcmis:nodeRef").getFirstValue() + "," + fileId);
		}
		

	}
	
	public static String findBoxFileId(BoxAPIConnection api, String path, String name, BoxFolder folder) {
		String fileId = null;
		String delim = "/";
		if (folder == null) {
			folder = BoxFolder.getRootFolder(api);
		}

		if (path != null && path.length() >= 1) {
			int index = path.indexOf(delim);
			String nFolder = path;
			if (index != -1) {
				nFolder = path.substring(0, index);
			}
			 System.out.println("nFolder: "+nFolder);
			Iterable<BoxItem.Info> info = folder.getChildren();
			for (BoxItem.Info i : info) {
				String bName = i.getName();
				 System.out.println("bName: "+bName);
				if (bName.equals(nFolder)) {
					if (path.length() > index + 1) {
						BoxFolder bFolder = new BoxFolder(api, i.getID());
						return findBoxFileId(api, index == -1 ? "" : path.substring(index + 1), name, bFolder);
					}

				}
			}
		} else {
			Iterable<BoxItem.Info> info = folder.getChildren();
			for (BoxItem.Info i : info) {
				String bName = i.getName();
				if (bName.equals(name)) {
					 System.out.println("File ID: " + i.getID() + " for " + name);
					return i.getID();
				}
			}
		}

		return fileId;
	}
	
	/**
	 * Recursively get a box folder id given a path and file name.
	 * 
	 * @param api
	 * @param path
	 * @param name
	 * @param folder
	 * @return
	 */
	public static String findBoxFolderId(BoxAPIConnection api, String path, BoxFolder folder, Node currentNode) {
		String folderId = null;
		String delim = "/";
		if (folder == null) {
			folder = BoxFolder.getRootFolder(api);
		}
		int index = path.indexOf(delim);
		String nFolder = path;
		if (index != -1) {
			nFolder = path.substring(0, index);
		}
		Iterable<BoxItem.Info> info = folder.getChildren();
		for (BoxItem.Info i : info) {
			String bName = i.getName();

			Node n = currentNode.findNode(bName);
			if (n == null) {
				n = new Node(bName, i.getID(), null);
				currentNode.addNode(n);
			}

			if (bName.equals(nFolder)) {
				if (path.length() > index + 1) {
					BoxFolder bFolder = new BoxFolder(api, i.getID());
					if (index == -1) {
						return i.getID();
					} else {
						return findBoxFolderId(api, path.substring(index + 1), bFolder, n);
					}
				}
			}
		}
		return folderId;
		// folder doesn't exist yet, create it
//		BoxItem.Info i = folder.createFolder(nFolder);
//
//		Node n = new Node(nFolder, i.getID(), folder);
//		currentNode.addNode(n);
//
//		// We might need to create more than one folder if the hierarchy
//		// doesn't exist yet
//		if (path.length() > index + 1) {
//			BoxFolder bFolder = new BoxFolder(api, i.getID());
//			if (index == -1) {
//				return i.getID();
//			} else {
//				return findBoxFolderId(api, path.substring(index + 1), bFolder, n);
//			}
//		}
//		return i.getID();
	}
	
	/**
	 * Get all folders under the given folder.
	 * 
	 * @param session
	 * @param f
	 * @return
	 */
	public static ItemIterable<QueryResult> getFolders(Session session, AlfrescoFolder f) {
		// Get all folders under that folder
		String cql = "SELECT * FROM cmis:folder WHERE IN_FOLDER('" + f.getId() + "')";
		ItemIterable<QueryResult> results = session.query(cql, false);
		return results;
	}

	/**
	 * Get all files under the given folder.
	 * 
	 * @param session
	 * @param f
	 * @return
	 */
	public static ItemIterable<QueryResult> getFiles(Session session, AlfrescoFolder f) {
		// Get all documents under that folder
		String cql = "SELECT * FROM cmis:document WHERE IN_FOLDER('" + f.getId() + "')";
		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(ITEMS_PER_PAGE);

		ItemIterable<QueryResult> results = session.query(cql, false, oc);
		return results;
	}

	/**
	 * Get a CMIS session and return it.
	 * 
	 * @param user
	 * @param password
	 * @param url
	 * @param repositoryId
	 * @return
	 */
	public static Session connectCMIS(String user, String password, String url, String repositoryId) {
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();
		// parameter.put(SessionParameter.ATOMPUB_URL, url);
		// parameter.put(SessionParameter.BINDING_TYPE,
		// BindingType.ATOMPUB.value());
		parameter.put(SessionParameter.BROWSER_URL, url);
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.BROWSER.value());
		parameter.put(SessionParameter.AUTH_HTTP_BASIC, "true");
		parameter.put(SessionParameter.USER, user);
		parameter.put(SessionParameter.PASSWORD, password);
		parameter.put(SessionParameter.REPOSITORY_ID, repositoryId);
		parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");
		List<Repository> repositories = factory.getRepositories(parameter);
		Session session = repositories.get(0).createSession();
		System.out.println("Connected to repository:" + repositories.get(0).getId());
		return session;
	}
	
	public static BoxAPIConnection connectBox() throws IOException {
		try {
			final App a = new App();
			a.readAuthInfo();
			BoxAPIConnection api = new BoxAPIConnection(boxClientId, boxClientSecret, a.accessToken, a.refreshToken);
			api.addListener(new BoxAPIConnectionListener() {
				public void onRefresh(BoxAPIConnection api) {
					String newAccessToken = api.getAccessToken();
					String newrefreshToken = api.getRefreshToken();
					System.out.println("AccessToken: " + newAccessToken);
					System.out.println("RefreshToken: " + newrefreshToken);
					// a = new App();
					a.accessToken = newAccessToken;
					a.refreshToken = newrefreshToken;
					a.writeAuthInfo();
					// update new access and refresh token in DB/property
				}

				public void onError(BoxAPIConnection api, BoxAPIException error) {
					System.out.println("Error in Box account details. " + error.getMessage());
				}
			});
			System.out.println("Completed Box authentication");
			return api;
		} catch (BoxAPIException be) {
			System.out.println("BOX EXCEPTION: " + be.getResponse());
			throw be;
		} finally {
			writeMappingLocal(mapping, "mapping", true);
			//writeMappingLocal(mappingCases, "case-mapping", true);
			writeMappingLocal(mappingError, "error-mapping", true);
		}
	}
	
	public static void writeMappingLocal(Map<String, String> mapping, String filename, boolean append)
			throws IOException {
		if (mapping.size() > 0) {
			// String filename = "mapping";
			File file = new File(BOX_AUTH_FILE_PATH+filename+".csv");
			if (!append) {
				int i = 1;
				while (file.exists()) {
					file = new File(BOX_AUTH_FILE_PATH+filename+i+".csv");
					i++;
				}
			}
			FileOutputStream fos = new FileOutputStream(file, append);
			String mappingStr = mappingToString(mapping);

			fos.write(mappingStr.getBytes());
			fos.close();
		}
	}
	
	public static String mappingToString(Map<String, String> mapping) {
		StringBuffer sb = new StringBuffer();
		Set<String> keys = mapping.keySet();
		int length = keys.size();
		int i = 0;
		for (String key : keys) {
			sb.append(mapping.get(key));
			if (i < length) {
				sb.append("\n");
			}
			i++;
		}
		return sb.toString();
	}

}
