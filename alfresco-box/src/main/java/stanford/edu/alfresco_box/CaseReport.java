package stanford.edu.alfresco_box;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.Metadata;

public class CaseReport {
	public static String FILE_PATH = "C:\\Users\\Michael\\Downloads\\";
	public static String FILE_OUT = "CaseReport.csv";
	public static String[] HEADER = {"Box ID", "Case Name","Case Title","Permissions Statement","Case Study Access","Available to the GSB Community","Free Case","HBP Link","Case Center Link"};

	public static String BOX_URL = "https://stanford.app.box.com/file/";
	private static String boxClientId = "opuitnahtbvfefalzhzav9b9e2k1jnbr";
	private static String boxClientSecret = "GET5iaqeYIop7ETZSuu3tj6lz3zulMJK";
	
	public static void main(String[] args) throws IOException {
		// The box developer API key - first argument
		String boxKey = "";
		if (args.length > 0) {
			boxKey = args[0];
		}
		// The box path to run the report on - second argument
		String boxPath = "";
		if (args.length > 0) {
			boxPath = args[1];
		}
		String fileOut = "";
		if (args.length > 0) {
			fileOut = args[2];
			FILE_OUT = fileOut;
		}
		System.out.println("Running Case Report for "+boxPath);
		System.out.println("Using Box Key: "+boxKey);
		
		final App a = new App();
		BoxAPIConnection api = new BoxAPIConnection(boxKey);
		// Get the BoxFolder for the given path
		String folderId = a.findBoxFolderId(api, boxPath, null, null);
		BoxFolder folder = new BoxFolder(api, folderId);
		
		System.out.println(folder.getID());
		
		List<String[]> rows = runReport(api, folder);
		printReport(rows);
	}
	
	// if case has the HBP or Case Center link
	// show the Permissions and Case Study Access values 
    //    Case Name (not sure if Item ID should also be shown)
    //    Permissions Statement
    //    Case Study Access
    //    Available to the GSB Community
    //    Free Case
    //    HBP or Case Center link
	public static List<String[]> runReport(BoxAPIConnection api, BoxFolder folder) {
		List<String[]> retVal = new ArrayList<String[]>();
		
		System.out.println(folder.getID());
		
		int i = 0;
		for (BoxItem.Info itemInfo : folder) {
			i++;
			System.out.println(itemInfo.getName());
		    if (itemInfo instanceof BoxFile.Info) {
		        BoxFile.Info fileInfo = (BoxFile.Info) itemInfo;
		        String fileId = fileInfo.getID();
		        BoxFile file = new BoxFile(api, fileId);
		        // Only pdf has the case metadata template
		        Iterable<Metadata> fileMetadatas = file.getAllMetadata();
		        if(fileMetadatas.iterator().hasNext()) {
			        Metadata fileMetadata = file.getMetadata("gsbitops");
//		        	System.out.println(fileMetadata.getTemplateName());
			        try {
			        	String[] row = new String[9];
//				        Metadata fileMetadata = file.getMetadata();
				        System.out.println(fileMetadata.get("/title"));
				        System.out.println(fileMetadata.get("/permissionsStatement"));
				        System.out.println(fileMetadata.get("/caseStudyAccess1"));
				        System.out.println(fileMetadata.get("/availableToTheGsbCommunity"));
				        System.out.println(fileMetadata.get("/freeCase1"));
				        System.out.println(fileMetadata.get("/linkAtHarvard"));
				        System.out.println(fileMetadata.get("/theCaseCenterLink"));
					    System.out.println(i+": "+fileInfo.getName());
					    int c = 0;
					    row[c++] = fileId;
					    row[c++] = fileInfo.getName();
					    row[c++] = fileMetadata.get("/title");
				        row[c++] = fileMetadata.get("/permissionsStatement");
				        row[c++] = fileMetadata.get("/caseStudyAccess1");
				        row[c++] = fileMetadata.get("/availableToTheGsbCommunity");
				        row[c++] = fileMetadata.get("/freeCase1");
				        row[c++] = fileMetadata.get("/linkAtHarvard");
				        row[c++] = fileMetadata.get("/theCaseCenterLink");
				        // Stanford GSB classes only, restricted
				        // Stanford GSB classes only, accessible
				        // Educational Use only
				        // Not for Distribution
 				        // AND 
				        // HBP and/or Case Center link exists
//				        if((row[7] != null || row[8] != null)
//				        		&&
//				        		(row[4].equals("Not for Distribution") ||
//				        				row[4].equals("Stanford GSB classes only, accessible") ||
//				        				row[4].equals("Stanford GSB classes only, restricted") ||
//				        				row[4].equals("Educational Use Only")) ) {
				        	retVal.add(row);
//				        } else {
				        	for(int x = 0; x < row.length; x++) {
				        		System.out.println(row[x]);
				        	}
//				        }
				        } catch(BoxAPIException e) {
				        	e.getMessage();
				        	printReport(retVal);
				        }
		        	
		        }
		    } else if (itemInfo instanceof BoxFolder.Info) {
		        BoxFolder.Info folderInfo = (BoxFolder.Info) itemInfo;
		        System.out.println(folderInfo.getName());
		        if(!folderInfo.getName().equals("M") &&
		        		!folderInfo.getName().equals("E") &&
		        		!folderInfo.getName().equals("OB") &&
		        		!folderInfo.getName().equals("OIT") &&
		        		!folderInfo.getName().equals("SI") &&
		        		!folderInfo.getName().equals("RE") &&
		        		!folderInfo.getName().equals("BP") &&
		        		!folderInfo.getName().equals("T") &&
		        		!folderInfo.getName().equals("SB") &&
		        		!folderInfo.getName().equals("F") &&
		        		!folderInfo.getName().equals("A") &&
		        		!folderInfo.getName().equals("IB") &&
		        		!folderInfo.getName().equals("GS") &&
		        		!folderInfo.getName().equals("P") &&
		        		!folderInfo.getName().equals("SM")
		        		) {
		        	List<String[]> subrows = runReport(api, new BoxFolder(api, folderInfo.getID()));
		        	retVal.addAll(subrows);
		        }
		        // Do something with the folder.
		    }
		}
		return retVal;
	}
	
	public static void printReport(List<String[]> rows) {
		
		try {
			FileWriter fw = new FileWriter(FILE_PATH + FILE_OUT);
			CSVPrinter printer = new CSVPrinter(fw, CSVFormat.EXCEL);
			printer.printRecord(HEADER);
			for(String[] s : rows) {
				printer.printRecord(s);
			}
			printer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

