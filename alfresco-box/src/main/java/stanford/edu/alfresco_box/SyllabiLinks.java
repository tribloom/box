package stanford.edu.alfresco_box;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxSharedLink;

/**
 * ******************************************************************************************
 * 
 * Developer token: https://stanford.app.box.com/developers/console/app/366214/configuration
 * 
 * Box: https://stanford.app.box.com/folder/0
 * 
 * Compile:
 * maven pom.xml clean
 * maven pom.xom compile
 * 
 * ******************************************************************************************
 * 
 * Add "shared links" to a folder in Box. This will be shared within "my organization" only.
 * This was originally written to add links to syllabi so that they may be viewed.
 * @author Michael
 *
 */
public class SyllabiLinks {
	/**
	 * Add shared links to files within a folder in Box. 
	 * @param args 
	 * 		Box API Key
	 * 		Box file path to folder
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String boxKey = "";
		if (args.length > 0) {
			boxKey = args[0];
		}
		String boxPath = "";
		if (args.length > 0) {
			boxPath = args[1];
		}
		System.out.println("Adding symbolic links to "+boxPath);
		System.out.println("Using Box Key: "+boxKey);
		final App a = new App();
		BoxAPIConnection api = new BoxAPIConnection(boxKey);
		api.setBaseURL("https://api.box.com/2.0/");
		String folderId = a.findBoxFolderId(api, boxPath, null, null);
		BoxFolder folder = new BoxFolder(api, folderId);
		
		List<String[]> outputAr = new ArrayList<String[]>();
				
		int i = 0;
		for (BoxItem.Info itemInfo : folder) {
			i++;
			//if (i < 17) { continue; }
		    if (itemInfo instanceof BoxFile.Info) {
		    	// The row in the CSV file for each Box file.
		    	String[] row = new String[13];
		    	
		        BoxFile.Info fileInfo = (BoxFile.Info) itemInfo;
//		        System.out.println(i+": "+fileInfo.getName());
		        // Do something with the file.
		        BoxSharedLink link = fileInfo.getSharedLink();
		        // new
		        //System.out.println("link: "+link);
		        //if (link == null || link.toString().length() == 0) {
			     //   System.out.println("Shared Link: "+link.getURL());
		        // end new
			    createSharedLink(api, fileInfo.getID());
		        // new 
		        //}
		        // end new
			    System.out.println(i+": "+fileInfo.getName());
		    } else if (itemInfo instanceof BoxFolder.Info) {
		        BoxFolder.Info folderInfo = (BoxFolder.Info) itemInfo;
		        // Do something with the folder.
		        // Currently we do not recurse on sub-folders
		    }
		}
	}
	
	public static void createSharedLink(BoxAPIConnection api, String fileId) {
	    BoxFile file = new BoxFile(api, fileId);
	    BoxSharedLink.Permissions permissions = new BoxSharedLink.Permissions();
	    permissions.setCanDownload(true);
	    permissions.setCanPreview(true);
	    BoxSharedLink sharedLink = file.createSharedLink(BoxSharedLink.Access.COMPANY, null, permissions);
	    System.out.println("Shared Link: "+sharedLink.getURL());
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
